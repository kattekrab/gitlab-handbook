module gitlab.com/internal-handbook/internal-handbook.gitlab.io

go 1.19

require (
	github.com/google/docsy v0.8.0 // indirect
	github.com/google/docsy/dependencies v0.7.2 // indirect
	github.com/hairyhenderson/go-codeowners v0.3.1-0.20230710165731-859d0d06aff1 // indirect
	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.3.10 // indirect
)
