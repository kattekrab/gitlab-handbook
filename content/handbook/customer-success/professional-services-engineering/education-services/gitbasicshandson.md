---
title: "GitLab with Git Essentials - Hands-On Lab"
description: "This Hands-On Guide walks you through the lab exercises used in the GitLab with Git Essentials course."
---

# GitLab with Git Essentials

## Lab Guides

> **We are transitioning to the latest version of this course.** If your group URL starts with `https://spt.gitlabtraining.cloud`, please use the v15 instructions.

 Lab Guide | Version 15 | Version 16
-----------|------------|------------
 Lab 1: Create a project and an issue | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab1/)
 Lab 2: Work with Git locally | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab2/)
 Lab 3: Use GitLab to merge code | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab3/)
 Lab 4: Build a `.gitlab-ci.yml` file | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab4.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab4/)
 Lab 5: Auto DevOps with a predefined project template | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab5/)
 Lab 6: Static Application Security Testing (SAST) | [v15 Lab Guide](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6.md) | [v16 Lab Guide](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandsonlab6/)

## Quick links

* [GitLab with Git Essentials course description](https://about.gitlab.com/services/education/gitlab-basics/)
* [GitLab Certified Associate Certification Details](https://about.gitlab.com/services/education/gitlab-certified-associate/)

## Suggestions?

If you’d like to suggest changes to the *GitLab with Git Essentials Hands-on Guide*, please submit them via merge request.
