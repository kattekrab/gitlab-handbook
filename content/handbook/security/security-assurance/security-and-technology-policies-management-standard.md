---
title: "Security and Technology Policies Management"
controlled_document: true
---

## Purpose

This policy is intended to establish requirements for the creation and management of security and technology related policies.

## Scope

This policy applies to security and technology policies that fall within the scope of GitLab's security compliance audits and assessments.

## Roles & responsibilities:

| Role  | Responsibility |
|-----------|-----------|
| Security Governance Team | Responsible for conducting annual controlled documents review
| Security Assurance Management (Code Owners) | Responsible for approving changes to this policy |

## Policy

### Policy creation and requirements

All in-scope policies must be created as version controlled documents in GitLab.

All in-scope policies must be listed as [Controlled Documents]({{< ref "controlled-document-procedure" >}}) utilizing appropriate frontmatter.

At a minimum, all in-scope policies must include a purpose, scope, and roles and responsibilities.

All policy statements for in-scope policies must be mapped to the appropriate [GCF]({{< ref "sec-controls" >}}) control(s).

### Policy review and approval

All in-scope policies must be reviewed and approved by appropriate stakeholders prior to merging the initial MR to create the policy.

In-scope policies must be added to the CODEOWNERS file with appropriate stakeholders listed as codeowners.

All in-scope policies must be reviewed and approved by appropriate stakeholders on at least an annual basis in accordance with the [Controlled Document Procedure]({{< ref "controlled-document-procedure" >}}).

### Policy communication and training

New and updated policies must be communicated to relevant team members upon creation or material update.

Relevant in-scope policies must be acknowledged by team members during onboarding training and annually thereafter.

